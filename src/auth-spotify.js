const opn = require('opn');
var SpotifyWebApi = require('spotify-web-api-node');
const jsonfile = require('jsonfile');
const config = require('../config.json');

class AuthSpotify {

    constructor() {
        this.spotifyApi = new SpotifyWebApi({
            redirectUri: config.redirectUri,
            clientId: config.clientId,
            clientSecret: config.clientSecret
        });
    }

    async connectWebApi() {

        try {
            const auth = jsonfile.readFileSync('auth.json');

            this.spotifyApi.setAccessToken(auth['access_token']);
            this.spotifyApi.setRefreshToken(auth['refresh_token']);

            if (auth.expiry_date < new Date().valueOf())
                await this._refreshAuthTokens();
        } catch (err) {
            console.log(err);
            console.log('must create auth tokens');
            await this._createAuthTokens();
        }


        return this.spotifyApi;
    }

    async _refreshAuthTokens() {

        console.log('attempting to fresh tokens');
        try {
            let data = await this.spotifyApi.refreshAccessToken();
            this.spotifyApi.setAccessToken(data.body['access_token']);
            data.body['refresh_token'] = this.spotifyApi.getRefreshToken();
            this._saveAuth(data);
            console.log('refreshed token');
        } catch (err) {
            console.log('wtf', err);
        }

        return;
    }
    _createAuthTokens() {

        return new Promise((res, rej) => {

            const express = require('express');
            const app = express();
            const port = 8888;
            let server;

            app.get('/callback', (request, response) => {
                const code = request.query.code;
                this.spotifyApi.authorizationCodeGrant(code)
                    .then((data) => {

                        this._saveAuth(data);
                        this.spotifyApi.setAccessToken(data.body['access_token']);
                        this.spotifyApi.setRefreshToken(data.body['refresh_token']);

                        server.close();
                        res();

                    }, function (err) {
                        console.log('Something went wrong!', err);
                    });

                response.send('Hello from Express!')
            });

            server = app.listen(port, (err) => {
                opn(this.spotifyApi.createAuthorizeURL(config.scopes, 'auth'));
            });
        })
    }

    _saveAuth(data) {
        data.body.expiry_date = new Date().valueOf() + (data.body['expires_in'] * 1000);
        jsonfile.writeFile('auth.json', data.body, { spaces: 2, EOL: '\r\n' });
    }

}

module.exports = new AuthSpotify();