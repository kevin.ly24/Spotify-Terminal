const blessed = require('blessed');

class PlayStatus {
    constructor() {
        this.widget = blessed.text({
            content: '',
            shrink: true,
            left: 4,
            width: "shrink"
        })

        this.data = {};
    }

    update() {
        // this.liveTimer();
        this.widget.content = this.data.song;
        this.widget.content += `  (${this.formatTime(this.data.progress)}/${this.formatTime(this.data.duration)})`
        this.widget.content += `\n${this.data.artist}`
    }

    setData(obj) {
        if(obj.artist) 
            obj.artist = obj.artist.map(e => { return e.name }).join(', ');

        Object.assign(this.data, obj);
        this.update();
    }

    formatTime(time) {
        let simple = parseInt(time/1000);
        let sec = simple % 60 + "";
        let min = parseInt(simple/60);

        if (sec.length == 1)
            sec = '0' + sec;

        return `${min}:${sec}`;
    }
    
    liveTimer() {
        if (this.data.playing && !this.interval) {
            this.data.progress += 700;
            this.interval = setInterval(() => {
                this.data.progress += 1000;
                this.update();
            }, 1000);
        } else if (!this.data.playing && this.interval) {
            clearInterval(this.interval);
            delete this.interval;
        }
    }
}

module.exports = new PlayStatus();