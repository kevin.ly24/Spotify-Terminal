const ListView = require('../views/ListView');
let api = require('../../auth-spotify').spotifyApi;

class Playlists extends ListView {
    constructor() {
        super('Playlists');
    }

    async getItems() {
        let views = [];
        let playlists = await api.getUserPlaylists();

        views = playlists.body.items.map(e => { return this.buildPlaylistView(e) });
        this.view.setItems(views);

        // if (playlists.body.next) {
        //     let x = await api.nextPage(playlists.body.next);
        // }
    }

    buildPlaylistView(item) {
        let x = { title: item.name }
        return x;
    }
}

module.exports = Playlists;