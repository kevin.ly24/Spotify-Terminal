const ListView = require('../views/ListView');
const Playlists = require('./playlists');

class Base extends ListView {

    constructor() {
        super('base');


    }

    async getItems() {
        let views = [];

        let playlists = new Playlists();
        views.push(playlists);

        this.view.setItems(views);
    }
}

module.exports = new Base();