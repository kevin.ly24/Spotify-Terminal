class ViewUtils {
    constructor() {
        this.activeViews = [null, null, null];
    }

    get parent() { return this.activeViews[0]; };
    set parent(x) { this.activeViews[0] = x; }

    get current() { return this.activeViews[1]; }
    set current(panel) {
        this.activeViews[1] = panel;
        this.initView(panel).then(e => {
            this.preview = e.items[e.selected]._data;
        })
    }

    get preview() { return this.activeViews[2]; }
    set preview(panel) {
        this.activeViews[2] = panel;
        this.initView(panel);
    }

    async initView(panel) {
        let widget = await panel.constructView();
        this.main.append(widget);
        panel.appended = true;
        return panel.view.widget;
    }

    getActiveViewLength() {
        return this.activeViews.map(e => { return e; }).length;
    }

    getRatios() {
        if (this.parent && this.current && this.preview)
            return [0.3, 0.4, 0.3];
        else if (this.parent && this.current)
            return [0.4, 0.6, null];
        else if (this.current && this.preview)
            return [null, 0.6, 0.4];
        else return [null, 1, null];
    }


}

module.exports = new ViewUtils();