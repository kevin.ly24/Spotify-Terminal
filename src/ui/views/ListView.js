
const List = require('./widgets/list');

class ListView {
    constructor(title) {
        this.title = title;
        this.appended = false;
        this.fetch = true;
    }

    /**
     * Creates view
     */
   async constructView() {
        this.view = new List();
        let items = await this.getItems();
        return this.view.widget;
    }

    async getItems() {
        this.view.setItems([]);
    }
}

module.exports = ListView;