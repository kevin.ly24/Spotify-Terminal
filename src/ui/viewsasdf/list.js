const blessed = require('blessed');
const View = require('./view');

class List extends View {

    constructor(title) {
        super(title);
        this.widget = blessed.list({
            width: '100%',
            height: '100%',
            top: -1,
            left: -1,
            keys: true,
            vi: true,
            tags: true,
            // hidden : true, 
            items: [],
            border: {
                type: 'line'
            },
            style: {
                selected: {
                    fg: 'black',
                    bg: 'white'
                },
                fg: 'white',
                border: {
                    fg: 'white'
                }
            }
        });

        // this.initActions();
    }

    

    setInActive() {
        this.widget.style.selected.bg = 'grey';
    }

    noActive() {
        this.widget.style.selected.bg = 'black';
        this.widget.style.selected.fg = 'white';
    }

    setActive() {
        this.widget.style.selected.bg = 'white';
    }

}


module.exports = List;