const blessed = require("blessed");

class SongList {

    constructor() {

        this.widget = blessed.list({
            width: '100%',
            height: '100%',
            top: -1,
            left: -1,
            keys: true,
            vi: true,
            tags: true,
            items: [],
            border: {
                type: 'line'
            },
            style: {
                selected: {
                    fg: 'black',
                    bg: 'white'
                },
                fg: 'white',
                border: {
                    fg: 'white'
                }
            }
        });

        util = new util(this.widget, 'songlist');

        this.songs = [];

        this.widget.on('focus', () => {
        });
    }

    displaySongs() {
        let display = this.songs.map(e => {
            this.widget.addItem("");
            let songitem = this.widget.items.slice(-1)[0];
            songitem._data = e;
            
            songitem.on('prerender', function () {
                this.setContent(scale(this));
            });

            songitem.on('keypress', function(ch, key) {
                util.error('pressed key ' + key)
            })
        });

        function scale(widget) {
            let song = widget._data.track.name;
            let artist = widget._data.track.artists.map(e => { return e.name }).join(', ');

            let width = widget.parent.width
            let songmaxlength = width * 0.6;
            let artistmaxlength = width - songmaxlength;

            let songwidth = songmaxlength - song.length
            if (songwidth > 0)
                song = song + " ".repeat(songwidth);
            else 
                song = song.substring(0, song.length + songwidth - 2) + '… ';

            let artistwidth = artistmaxlength - artist.length
            if (artistwidth < 0) 
                artist = artist.substring(0, artist.length + artistwidth - 3) + '…';

            return song + artist;
        }


        util.render();

        return display;
    }
}

module.exports = SongList;