# Spotify Terminal Controller

This is currently in alpha.

This is an ncurses based application used to control a Spotify music playing device registered on to an account. The interface is inspired by
rangerfm.

![GitHub Logo](demo.png)

## Current functionality
- Auto Spotify authentication with expressjs
- Working status bar
- Play and pause
- Navigating between playlists and songs

## Caveats
- Nested folder structures of playlists dont show up in the Spotify API
