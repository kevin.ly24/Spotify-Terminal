let spotify = require('./src/auth-spotify');
process.on('unhandledRejection', (reason, p) => { throw reason });

async function test() {
    let api = await spotify.connectWebApi();


    var global = require('./src/global');
    var blessed = require('blessed');

    // Create a screen object.
    var screen = blessed.screen({
        smartCSR: true,
        dockBorders: true,
        log: 'test.log'
    });

    screen.title = 'my window title';

    let statusbar = require('./src/ui/status-bar');
    let viewer = require('./src/ui/viewer');

    screen.append(statusbar.widget);
    screen.append(viewer.widget);

    // Quit on Escape, q, or Control-C.
    screen.key(['escape', 'q', 'C-c'], function (ch, key) {
        return process.exit(0);
    });

    // Render the screen.
    screen.render();

    setInterval(() => screen.render(), 1000);

    // screen.on('blur', () => screen.log('blur'));
    // screen.on('focus', () => screen.log('focused'));
}

test();


